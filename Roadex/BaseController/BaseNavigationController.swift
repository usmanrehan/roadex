//
//  BaseNavigationController.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 30/07/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.barTintColor = UIColor.black
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationBar.isTranslucent = false
    }

}
