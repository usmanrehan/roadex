//
//  BaseViewController.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 03/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var notificationBarItem: UIBarButtonItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = Global.APP_COLOR
        
        guard let navigation = self.navigationController else {return}
        if (navigation.viewControllers.count) > 1 {
            self.addBackBarButtonItem()
        }
        // Do any additional setup after loading the view.
    }
    
    func addBackBarButtonItem() {
        let image = #imageLiteral(resourceName: "ic_back").withRenderingMode(.alwaysOriginal)
        let backItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(backWithPop))
        self.navigationItem.leftBarButtonItem = backItem
    }
    
    @objc func backWithPop() {
        if (self.navigationController?.viewControllers.count)! > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
