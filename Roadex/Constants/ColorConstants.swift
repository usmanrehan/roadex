//
//  ColorConstants.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/9/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
struct ColorConstants {
    static let NAVIGATION_BAR = UIColor(hexString: "#000000")
    static let APP_COLOR = UIColor(hexString: "#F26303")
    static let BLUE = UIColor(hexString: "#009FD1")
    static let RED = UIColor(hexString: "#EA421F")
    static let YELLOW = UIColor(hexString: "#FFAD00")
    static let GREY = UIColor(hexString: "#909598")
}
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
