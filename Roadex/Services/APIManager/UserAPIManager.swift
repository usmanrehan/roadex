//
//  UserAPIManager.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    //MARK:- LOG IN
    func login(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Constants.BaseURL)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Get Array Response
    func getArrayResponse(endPoint:String,params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route: endPoint, params: params)! as URL
        print(route)
        self.getRequestForArrayWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- Get Dictionary Response
    func getDictionaryResponse(endPoint:String,params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route: endPoint, params: params)! as URL
        print(route)
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- Post Array Response
    func postArrayResponse(endPoint:String,params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route: endPoint, params: params)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Post Dictionary Response
    func postDictionaryResponse(endPoint:String,params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: endPoint)! as URL
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
}
