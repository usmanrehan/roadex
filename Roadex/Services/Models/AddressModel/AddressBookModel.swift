//
//  AddressBookModel.swift
//
//  Created by Hamza Hasan on 08/02/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ShipperAddressBookModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let wpcargoShipperName = "wpcargo_shipper_name"
    static let wpcargoShipperAddress = "wpcargo_shipper_address"
    static let wpcargoShipperCity = "wpcargo_shipper_city"
    static let wpcargoShipperPhone = "wpcargo_shipper_phone"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var wpcargoShipperName: String? = ""
  @objc dynamic var wpcargoShipperAddress: String? = ""
  @objc dynamic var wpcargoShipperCity: String? = ""
  @objc dynamic var wpcargoShipperPhone: String? = ""
  @objc dynamic var id: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    wpcargoShipperName <- map[SerializationKeys.wpcargoShipperName]
    wpcargoShipperAddress <- map[SerializationKeys.wpcargoShipperAddress]
    wpcargoShipperCity <- map[SerializationKeys.wpcargoShipperCity]
    wpcargoShipperPhone <- map[SerializationKeys.wpcargoShipperPhone]
    id <- map[SerializationKeys.id]
  }
}

public class ReceiverAddressBookModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let wpcargoReceiverName = "wpcargo_receiver_name"
        static let wpcargoReceiverAddress = "wpcargo_receiver_address"
        static let wpcargoReceiverAddress2 = "wpcargo_receiver_address2"
        static let wpcargoReceiverCity = "wpcargo_receiver_city"
        static let wpcargoReceiverPhone = "wpcargo_receiver_phone"
        static let id = "id"
    }
    
    // MARK: Properties
    @objc dynamic var wpcargoReceiverName: String? = ""
    @objc dynamic var wpcargoReceiverAddress: String? = ""
    @objc dynamic var wpcargoReceiverAddress2: String? = ""
    @objc dynamic var wpcargoReceiverCity: String? = ""
    @objc dynamic var wpcargoReceiverPhone: String? = ""
    @objc dynamic var id: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        wpcargoReceiverName <- map[SerializationKeys.wpcargoReceiverName]
        wpcargoReceiverAddress <- map[SerializationKeys.wpcargoReceiverAddress]
        wpcargoReceiverAddress2 <- map[SerializationKeys.wpcargoReceiverAddress2]
        wpcargoReceiverCity <- map[SerializationKeys.wpcargoReceiverCity]
        wpcargoReceiverPhone <- map[SerializationKeys.wpcargoReceiverPhone]
        id <- map[SerializationKeys.id]
    }
}
