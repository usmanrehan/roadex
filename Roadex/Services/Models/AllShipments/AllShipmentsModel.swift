//
//  AllShipments.swift
//
//  Created by Hamza Hasan on 07/02/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class AllShipmentsModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let wpcargoShipperAddress = "wpcargo_shipper_address"
    static let shipmentImages = "shipment_images"
    static let wpcargoExpectedDeliveryDatePicker = "wpcargo_expected_delivery_date_picker"
    static let wpcargoReceiverAddress = "wpcargo_receiver_address"
    static let cashOnDel = "cash_on_del"
    static let stops = "stops"
    static let registeredShipper = "registered_shipper"
    static let wpcargoShipperPhone = "wpcargo_shipper_phone"
    static let wpcargoStatus = "wpcargo_status"
    static let iD = "ID"
    static let layover = "layover"
    static let pqPackageItems = "pq_package_items"
    static let shipmentPackages = "shipment_packages"
    static let wpcargoWeight = "wpcargo_weight"
    static let postDate = "post_date"
    static let wpcargoReceiverName = "wpcargo_receiver_name"
    static let shipmentHistory = "shipment_history"
    static let wpcargoReceiverCity = "wpcargo_receiver_city"
    static let postDateGmt = "post_date_gmt"
    static let postName = "post_name"
    static let wpcargoCourier = "wpcargo_courier"
    static let wpcargoTypeOfShipment = "wpcargo_type_of_shipment"
    static let wpcargoShipperName = "wpcargo_shipper_name"
    static let wpcargoPickupTimePicker = "wpcargo_pickup_time_picker"
    static let postTitle = "post_title"
    static let wpcargoOrderNote3 = "wpcargo_order_note3"
    static let wpcargoReceiverPhone = "wpcargo_receiver_phone"
    static let wpcargoPickupDatePicker = "wpcargo_pickup_date_picker"
    static let wpcargoComments = "wpcargo_comments"
    static let postAuthor = "post_author"
    static let openShipment = "open_shipment"
    static let wpcargoDestination = "wpcargo_destination"
    static let fuel = "fuel"
    static let paymentWpcargoModeField = "payment_wpcargo_mode_field"
    static let wpcargoShipperCity = "wpcargo_shipper_city"
    static let wpcargoReceiverAddress2 = "wpcargo_receiver_address2"
    static let postModifiedGmt = "post_modified_gmt"
    static let wpcargoShipperEmail = "wpcargo_shipper_email"
    static let postModified = "post_modified"
    static let freight = "freight"
  }

  // MARK: Properties
  @objc dynamic var wpcargoShipperAddress: String? = ""
  @objc dynamic var shipmentImages: String? = ""
  @objc dynamic var wpcargoExpectedDeliveryDatePicker: String? = ""
  @objc dynamic var wpcargoReceiverAddress: String? = ""
  @objc dynamic var cashOnDel: String? = ""
  @objc dynamic var stops: String? = ""
  @objc dynamic var registeredShipper: String? = ""
  @objc dynamic var wpcargoShipperPhone: String? = ""
  @objc dynamic var wpcargoStatus: String? = ""
  @objc dynamic var iD: String? = ""
  @objc dynamic var layover: String? = ""
  var pqPackageItems = List<PqPackageItems>()
  @objc dynamic var shipmentPackages: String? = ""
  @objc dynamic var wpcargoWeight: String? = ""
  @objc dynamic var postDate: String? = ""
  @objc dynamic var wpcargoReceiverName: String? = ""
  var shipmentHistory = List<ShipmentHistoryModel>()
  @objc dynamic var wpcargoReceiverCity: String? = ""
  @objc dynamic var postDateGmt: String? = ""
  @objc dynamic var postName: String? = ""
  @objc dynamic var wpcargoCourier: String? = ""
  @objc dynamic var wpcargoTypeOfShipment: String? = ""
  @objc dynamic var wpcargoShipperName: String? = ""
  @objc dynamic var wpcargoPickupTimePicker: String? = ""
  @objc dynamic var postTitle: String? = ""
  @objc dynamic var wpcargoOrderNote3: String? = ""
  @objc dynamic var wpcargoReceiverPhone: String? = ""
  @objc dynamic var wpcargoPickupDatePicker: String? = ""
  @objc dynamic var wpcargoComments: String? = ""
  @objc dynamic var postAuthor: String? = ""
  @objc dynamic var openShipment: String? = ""
  @objc dynamic var wpcargoDestination: String? = ""
  @objc dynamic var fuel: String? = ""
  @objc dynamic var paymentWpcargoModeField: String? = ""
  @objc dynamic var wpcargoShipperCity: String? = ""
  @objc dynamic var wpcargoReceiverAddress2: String? = ""
  @objc dynamic var postModifiedGmt: String? = ""
  @objc dynamic var wpcargoShipperEmail: String? = ""
  @objc dynamic var postModified: String? = ""
  @objc dynamic var freight: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
        self.init()
    }

    override public class func primaryKey() -> String? {
    return "iD"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    wpcargoShipperAddress <- map[SerializationKeys.wpcargoShipperAddress]
    shipmentImages <- map[SerializationKeys.shipmentImages]
    wpcargoExpectedDeliveryDatePicker <- map[SerializationKeys.wpcargoExpectedDeliveryDatePicker]
    wpcargoReceiverAddress <- map[SerializationKeys.wpcargoReceiverAddress]
    cashOnDel <- map[SerializationKeys.cashOnDel]
    stops <- map[SerializationKeys.stops]
    registeredShipper <- map[SerializationKeys.registeredShipper]
    wpcargoShipperPhone <- map[SerializationKeys.wpcargoShipperPhone]
    wpcargoStatus <- map[SerializationKeys.wpcargoStatus]
    iD <- map[SerializationKeys.iD]
    layover <- map[SerializationKeys.layover]
    pqPackageItems <- (map[SerializationKeys.pqPackageItems], ListTransform<PqPackageItems>())
    shipmentPackages <- map[SerializationKeys.shipmentPackages]
    wpcargoWeight <- map[SerializationKeys.wpcargoWeight]
    postDate <- map[SerializationKeys.postDate]
    wpcargoReceiverName <- map[SerializationKeys.wpcargoReceiverName]
    shipmentHistory <- map[SerializationKeys.shipmentHistory]
    wpcargoReceiverCity <- map[SerializationKeys.wpcargoReceiverCity]
    postDateGmt <- map[SerializationKeys.postDateGmt]
    postName <- map[SerializationKeys.postName]
    wpcargoCourier <- map[SerializationKeys.wpcargoCourier]
    wpcargoTypeOfShipment <- map[SerializationKeys.wpcargoTypeOfShipment]
    wpcargoShipperName <- map[SerializationKeys.wpcargoShipperName]
    wpcargoPickupTimePicker <- map[SerializationKeys.wpcargoPickupTimePicker]
    postTitle <- map[SerializationKeys.postTitle]
    wpcargoOrderNote3 <- map[SerializationKeys.wpcargoOrderNote3]
    wpcargoReceiverPhone <- map[SerializationKeys.wpcargoReceiverPhone]
    wpcargoPickupDatePicker <- map[SerializationKeys.wpcargoPickupDatePicker]
    wpcargoComments <- map[SerializationKeys.wpcargoComments]
    postAuthor <- map[SerializationKeys.postAuthor]
    openShipment <- map[SerializationKeys.openShipment]
    wpcargoDestination <- map[SerializationKeys.wpcargoDestination]
    fuel <- map[SerializationKeys.fuel]
    paymentWpcargoModeField <- map[SerializationKeys.paymentWpcargoModeField]
    wpcargoShipperCity <- map[SerializationKeys.wpcargoShipperCity]
    wpcargoReceiverAddress2 <- map[SerializationKeys.wpcargoReceiverAddress2]
    postModifiedGmt <- map[SerializationKeys.postModifiedGmt]
    wpcargoShipperEmail <- map[SerializationKeys.wpcargoShipperEmail]
    postModified <- map[SerializationKeys.postModified]
    freight <- map[SerializationKeys.freight]
  }


}
