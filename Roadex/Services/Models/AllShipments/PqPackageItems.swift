//
//  PqPackageItems.swift
//
//  Created by Hamza Hasan on 07/02/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class PqPackageItems: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let descriptionValue = "description"
    static let height = "height"
    static let length = "length"
    static let width = "width"
    static let qty = "qty"
    static let pieceType = "piece_type"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var height: String? = ""
  @objc dynamic var length: String? = ""
  @objc dynamic var width: String? = ""
  @objc dynamic var qty: String? = ""
  @objc dynamic var pieceType: String? = ""
  @objc dynamic var id: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    descriptionValue <- map[SerializationKeys.descriptionValue]
    height <- map[SerializationKeys.height]
    length <- map[SerializationKeys.length]
    width <- map[SerializationKeys.width]
    qty <- map[SerializationKeys.qty]
    pieceType <- map[SerializationKeys.pieceType]
    id <- map[SerializationKeys.id]
  }


}
