//
//  ShipmentHistoryModel.swift
//
//  Created by Hamza Hasan on 07/02/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ShipmentHistoryModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let date = "date"
    static let updatedBy = "updated-by"
    static let location = "location"
    static let time = "time"
    static let remarks = "remarks"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var status: String? = ""
  @objc dynamic var date: String? = ""
  @objc dynamic var updatedBy: String? = ""
  @objc dynamic var location: String? = ""
  @objc dynamic var time: String? = ""
  @objc dynamic var remarks: String? = ""
  @objc dynamic var id: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    date <- map[SerializationKeys.date]
    updatedBy <- map[SerializationKeys.updatedBy]
    location <- map[SerializationKeys.location]
    time <- map[SerializationKeys.time]
    remarks <- map[SerializationKeys.remarks]
    id <- map[SerializationKeys.id]
  }


}
