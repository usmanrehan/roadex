//
//  User.swift
//
//  Created by Hamza Hasan on 06/02/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class User: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let cargoApi = "cargo_api"
        static let iD = "ID"
        static let userRegistered = "user_registered"
        static let userActivationKey = "user_activation_key"
        static let userEmail = "user_email"
        static let userLogin = "user_login"
        static let displayName = "display_name"
        static let userNicename = "user_nicename"
        static let userStatus = "user_status"
        static let userUrl = "user_url"
        static let userPass = "user_pass"
    }
    
    // MARK: Properties
    @objc dynamic var cargoApi: String? = ""
    @objc dynamic var iD: String? = ""
    @objc dynamic var userRegistered: String? = ""
    @objc dynamic var userActivationKey: String? = ""
    @objc dynamic var userEmail: String? = ""
    @objc dynamic var userLogin: String? = ""
    @objc dynamic var displayName: String? = ""
    @objc dynamic var userNicename: String? = ""
    @objc dynamic var userStatus: String? = ""
    @objc dynamic var userUrl: String? = ""
    @objc dynamic var userPass: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "iD"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        cargoApi <- map[SerializationKeys.cargoApi]
        iD <- map[SerializationKeys.iD]
        userRegistered <- map[SerializationKeys.userRegistered]
        userActivationKey <- map[SerializationKeys.userActivationKey]
        userEmail <- map[SerializationKeys.userEmail]
        userLogin <- map[SerializationKeys.userLogin]
        displayName <- map[SerializationKeys.displayName]
        userNicename <- map[SerializationKeys.userNicename]
        userStatus <- map[SerializationKeys.userStatus]
        userUrl <- map[SerializationKeys.userUrl]
        userPass <- map[SerializationKeys.userPass]
    }
}
