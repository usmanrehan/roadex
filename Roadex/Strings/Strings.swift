//
//  Strings.swift
//  Arbaeen
//
//  Created by M Usman Bin Rehan on 03/11/2018.
//  Copyright © 2018 Travel. All rights reserved.
//

import Foundation
enum Strings:String{
    case ERROR = "Error"
    case INVALID_USER_NAME = "Please enter valid username."
    case INVALID_PASSWORD = "Please enter valid password of atleast 6 characters."
    case INVALID_PASSWORD_AND_CONFIRM_PASSWORD = "Password and confirm password didn't match."
    case INVALID_EMAIL = "Please enter valid email address."
    case INVALID_PHONE = "Please enter valid phone number."
    case INVALID_ADDRESS = "Please enter your complete address."
    case INVALID_CITY = "Please enter your city name."
    case IMPLEMENT_LATER = "Will be implemented later."
    case INVALID_TRACKING_NUMBER = "Please enter valid tracking number."
    case INVALID_INFORMATION_PROVIDE = "Please enter valid information."
    
    case DASHBOARD = "DASHBOARD"
    case ALL_SHIPMENT = "ALL SHIPMENT"
    case ADDRESS_BOOK = "ADDRESS BOOK"
    case TRACKING = "TRACKING"
    case LOGOUT = "LOGOUT"
}
