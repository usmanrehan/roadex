//
//  AddAddress.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 09/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

protocol RefreshAddressList {
    func refreshAddressList()
}

class AddAddress: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfShipperName: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfAddress2: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var viewAddress2: UIView!
    @IBOutlet weak var dropDownAnchor: UIView!
    
    var isAddAddress = true
    var addAddressType = AddressBookType.Shipper
    var shipperAddressBookModel = ShipperAddressBookModel()
    var receiverAddressBookModel = ReceiverAddressBookModel()
    var delegate:RefreshAddressList?
    var arrCities = [String]()
    let dropDown = DropDown()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCities()
        self.setUI()
    }
    
    @IBAction func onBtnCross(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnCitySelect(_ sender: UIButton) {
        self.dropDown.show()
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        if self.isAddAddress{
            switch self.addAddressType{
            case .Shipper:
                self.addShipperAddress()
            case .Receiver:
                self.addReceiverAddress()
            }
        }
        else{
            switch self.addAddressType{
            case .Shipper:
                self.updateShipperAddress()
            case .Receiver:
                self.updateReceiverAddress()
            }
        }
    }
}
extension AddAddress{
    private func setUI(){
        self.setDropDownAnchor()
        switch self.addAddressType {
        case .Shipper:
            self.lblTitle.text = "Add shipper address"
            self.viewAddress2.isHidden = true
            self.tfAddress2.isUserInteractionEnabled = false
            if !self.isAddAddress{
                self.lblTitle.text = "Update shipper address"
                let data = self.shipperAddressBookModel
                self.tfPhoneNumber.text = data.wpcargoShipperPhone
                self.tfShipperName.text = data.wpcargoShipperName
                self.tfAddress.text = data.wpcargoShipperAddress
                self.tfCity.text = data.wpcargoShipperCity
            }
            else{
                self.tfCity.text = self.arrCities.first
            }
        case .Receiver:
            self.lblTitle.text = "Add receiver address"
            if !self.isAddAddress{
                self.lblTitle.text = "Update receiver address"
                let data = self.receiverAddressBookModel
                self.tfPhoneNumber.text = data.wpcargoReceiverPhone
                self.tfShipperName.text = data.wpcargoReceiverName
                self.tfAddress.text = data.wpcargoReceiverAddress
                self.tfAddress2.text = data.wpcargoReceiverAddress2
                self.tfCity.text = data.wpcargoReceiverCity
            }
            else{
                self.tfCity.text = self.arrCities.first
            }
        }
        
    }
    private func setDropDownAnchor(){
        self.dropDown.anchorView = self.dropDownAnchor
        self.dropDown.dataSource = self.arrCities
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfCity.text = self.arrCities[index]
        }
    }
}
//MARK:- Services
extension AddAddress{
    private func addShipperAddress(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/address/shipper/add"
        let wpcargo_shipper_phone = self.tfPhoneNumber.text ?? ""
        let wpcargo_shipper_name = self.tfShipperName.text ?? ""
        let wpcargo_shipper_address = self.tfAddress.text ?? ""
        let wpcargo_shipper_city = self.tfCity.text ?? ""
        let params:[String:Any] = ["wpcargo_shipper_phone":wpcargo_shipper_phone,
                                   "wpcargo_shipper_name":wpcargo_shipper_name,
                                   "wpcargo_shipper_address":wpcargo_shipper_address,
                                   "wpcargo_shipper_city":wpcargo_shipper_city]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.postDictionaryResponse(endPoint: endPoint, params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.dismiss(animated: true, completion: nil)
            self.delegate?.refreshAddressList()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func updateShipperAddress(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/address/shipper/update"
        let address_id = self.shipperAddressBookModel.id ?? "0"
        let wpcargo_shipper_phone = self.tfPhoneNumber.text ?? ""
        let wpcargo_shipper_name = self.tfShipperName.text ?? ""
        let wpcargo_shipper_address = self.tfAddress.text ?? ""
        let wpcargo_shipper_city = self.tfCity.text ?? ""
        let params:[String:Any] = ["address_id":address_id,
                                   "wpcargo_shipper_phone":wpcargo_shipper_phone,
                                   "wpcargo_shipper_name":wpcargo_shipper_name,
                                   "wpcargo_shipper_address":wpcargo_shipper_address,
                                   "wpcargo_shipper_city":wpcargo_shipper_city]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.postDictionaryResponse(endPoint: endPoint, params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.dismiss(animated: true, completion: nil)
            self.delegate?.refreshAddressList()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func addReceiverAddress(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/address/receiver/add"
        let wpcargo_receiver_phone = self.tfPhoneNumber.text ?? ""
        let wpcargo_receiver_name = self.tfShipperName.text ?? ""
        let wpcargo_receiver_address = self.tfAddress.text ?? ""
        let wpcargo_receiver_address2 = self.tfAddress2.text ?? ""
        let wpcargo_receiver_city = self.tfCity.text ?? ""
        let params:[String:Any] = ["wpcargo_receiver_phone":wpcargo_receiver_phone,
                                   "wpcargo_receiver_name":wpcargo_receiver_name,
                                   "wpcargo_receiver_address":wpcargo_receiver_address,
                                   "wpcargo_receiver_address2":wpcargo_receiver_address2,
                                   "wpcargo_receiver_city":wpcargo_receiver_city]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.postDictionaryResponse(endPoint: endPoint, params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.dismiss(animated: true, completion: nil)
            self.delegate?.refreshAddressList()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func updateReceiverAddress(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/address/receiver/update"
        let address_id = self.receiverAddressBookModel.id ?? "0"
        let wpcargo_receiver_phone = self.tfPhoneNumber.text ?? ""
        let wpcargo_receiver_name = self.tfShipperName.text ?? ""
        let wpcargo_receiver_address = self.tfAddress.text ?? ""
        let wpcargo_receiver_address2 = self.tfAddress2.text ?? ""
        let wpcargo_receiver_city = self.tfCity.text ?? ""
        let params:[String:Any] = ["address_id":address_id,
                                   "wpcargo_receiver_phone":wpcargo_receiver_phone,
                                   "wpcargo_receiver_name":wpcargo_receiver_name,
                                   "wpcargo_receiver_address":wpcargo_receiver_address,
                                   "wpcargo_receiver_address2":wpcargo_receiver_address2,
                                   "wpcargo_receiver_city":wpcargo_receiver_city]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.postDictionaryResponse(endPoint: endPoint, params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.dismiss(animated: true, completion: nil)
            self.delegate?.refreshAddressList()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getCities(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/shipment/fields"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getDictionaryResponse(endPoint: endPoint, params: [:], success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            if let receiver_info = response["receiver_info"] as? NSArray{
                for item in receiver_info{
                    guard let obj = item as? NSDictionary else {return}
                    guard let label = obj["label"] as? String else {return}
                    if label == "City"{
                        guard let cities = obj["field_data"] as? [String] else {return}
                        self.arrCities = cities
                        self.setUI()
                        break
                    }
                }
            }
            
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }

}

