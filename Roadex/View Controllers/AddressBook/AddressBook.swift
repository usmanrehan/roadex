//
//  AddressBookViewController.swift
//  Rodex
//
//  Created by Shardha Kanwal on 29/01/2019.
//  Copyright © 2019 Shardha Kanwal. All rights reserved.
//

import UIKit
import ObjectMapper

enum AddressBookType{
    case Shipper
    case Receiver
}

class AddressBook: BaseViewController {
    @IBOutlet weak var seperator1: UIView!
    @IBOutlet weak var seperator2: UIView!
    @IBOutlet weak var tableView: UITableView!
    var addressBookType = AddressBookType.Shipper
    var arrShipperAddressBookModel = [ShipperAddressBookModel]()
    var arrReceiverAddressBookModel = [ReceiverAddressBookModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getShipperBook()
    }
    
    @IBAction func onBtnSideMenu(_ sender: Any) {
        self.sideMenuController?.showLeftView()
    }
    @IBAction func onBtnAddAddress(_ sender: Any) {
        self.presentAddAddress()
    }
    @IBAction func onBtnShipperBook(_ sender: Any) {
        self.addressBookType = .Shipper
        self.getShipperBook()
    }
    @IBAction func onBtnReceiverBook(_ sender: Any) {
        self.addressBookType = .Receiver
        self.getReceiverBook()
    }
}
extension AddressBook : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.addressBookType {
        case .Shipper:
            return self.arrShipperAddressBookModel.count
        case .Receiver:
            return self.arrReceiverAddressBookModel.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipperTVC", for: indexPath) as! ShipperTVC
        switch self.addressBookType {
        case .Shipper:
            cell.setDataShipper(data: self.arrShipperAddressBookModel[indexPath.row])
        case .Receiver:
            cell.setDataReceiver(data: self.arrReceiverAddressBookModel[indexPath.row])
        }
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(self.onBtnEditAddress(sender:)), for: .touchUpInside)
        return cell
    }
}
extension AddressBook{
    private func setUI(){
        switch self.addressBookType {
        case .Shipper:
            self.seperator1.alpha = 1.0
            self.seperator2.alpha = 0.0
        case .Receiver:
            self.seperator1.alpha = 0.0
            self.seperator2.alpha = 1.0
        }
        self.tableView.reloadData()
    }
    private func presentAddAddress(){
        let storyboard = AppStoryboard.Main.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AddAddress") as! AddAddress
        controller.addAddressType = self.addressBookType
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    private func presentEditAddress(index:Int){
        let storyboard = AppStoryboard.Main.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AddAddress") as! AddAddress
        controller.isAddAddress = false
        controller.addAddressType = self.addressBookType
        controller.delegate = self
        switch self.addressBookType {
        case .Shipper:
            controller.shipperAddressBookModel = self.arrShipperAddressBookModel[index]
        case .Receiver:
            controller.receiverAddressBookModel = self.arrReceiverAddressBookModel[index]
        }
        self.present(controller, animated: true, completion: nil)
    }
    @objc func onBtnEditAddress(sender:UIButton){
        self.presentEditAddress(index: sender.tag)
    }
}
extension AddressBook{
    private func getShipperBook(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/address/shipper/all"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getArrayResponse(endPoint: endPoint, params: [:], success: { (responseArray) in
            Utility.hideLoader()
            self.arrShipperAddressBookModel = Mapper<ShipperAddressBookModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            print(self.arrShipperAddressBookModel)
            self.setUI()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getReceiverBook(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/address/receiver/all"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getArrayResponse(endPoint: endPoint, params: [:], success: { (responseArray) in
            print(responseArray)
            Utility.hideLoader()
            self.arrReceiverAddressBookModel = Mapper<ReceiverAddressBookModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            print(self.arrReceiverAddressBookModel.count)
            self.setUI()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
extension AddressBook:RefreshAddressList{
    func refreshAddressList() {
        switch self.addressBookType {
        case .Shipper:
            self.getShipperBook()
        case .Receiver:
            self.getReceiverBook()
        }
    }
}
