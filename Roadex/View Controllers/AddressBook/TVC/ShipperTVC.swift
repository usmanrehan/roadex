//
//  ShipperTableViewCell.swift
//  Rodex
//
//  Created by Shardha Kanwal on 30/01/2019.
//  Copyright © 2019 Shardha Kanwal. All rights reserved.
//

import UIKit

class ShipperTVC: UITableViewCell {
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblShipperTitle: UILabel!

    func setDataShipper(data:ShipperAddressBookModel){
        self.selectionStyle = .none
        self.lblShipperTitle.text = data.wpcargoShipperAddress ?? "-"
    }
    func setDataReceiver(data:ReceiverAddressBookModel){
        self.selectionStyle = .none
        self.lblShipperTitle.text = data.wpcargoReceiverAddress ?? "-"
    }
}
