//
//  AllShipmentsViewController.swift
//  Rodex
//
//  Created by Shardha Kanwal on 30/01/2019.
//  Copyright © 2019 Shardha Kanwal. All rights reserved.
//

import UIKit
import ObjectMapper

class AllShipments: BaseViewController {

    @IBOutlet weak var tblShipmentDetails: UITableView!
    var arrAllShipmentsModel = [AllShipmentsModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllShipments()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnSideMenu(_ sender: UIBarButtonItem) {
        self.sideMenuController?.showLeftView()
    }
}
extension AllShipments: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAllShipmentsModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllShipmentsTVC", for: indexPath) as! AllShipmentsTVC
        cell.setData(data: self.arrAllShipmentsModel[index])
        cell.btnViewDetail.tag = index
        cell.btnViewDetail.addTarget(self, action: #selector(self.onBtnViewDetail(sender:)), for: .touchUpInside)
        return cell
    }
    @objc func onBtnViewDetail(sender:UIButton){
        let index = sender.tag
        let data = self.arrAllShipmentsModel[index]
        self.pushToShipmentDetail(data: data)
    }
}
//MARK:- Helper Methods
extension AllShipments{
    private func pushToShipmentDetail(data:AllShipmentsModel){
        let storyboard = AppStoryboard.Main.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ShipmentDetail") as! ShipmentDetail
        controller.shipment = data
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- Service
extension AllShipments{
    private func getAllShipments(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/all"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getArrayResponse(endPoint: endPoint, params: [:], success: { (responseArray) in
            Utility.hideLoader()
            self.arrAllShipmentsModel = Mapper<AllShipmentsModel>().mapArray(JSONArray: responseArray as! [[String : Any]]) 
            print(self.arrAllShipmentsModel)
            self.tblShipmentDetails.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
