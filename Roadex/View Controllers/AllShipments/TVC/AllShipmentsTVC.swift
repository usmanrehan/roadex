//
//  AllShipmentsTableViewCell.swift
//  Rodex
//
//  Created by Shardha Kanwal on 30/01/2019.
//  Copyright © 2019 Shardha Kanwal. All rights reserved.
//

import UIKit

class AllShipmentsTVC: UITableViewCell {
    
    @IBOutlet weak var lblTrackingNumber: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCashOnDelivery: UILabel!
    @IBOutlet weak var btnViewDetail: UIButton!
    
    func setData(data:AllShipmentsModel){
        self.selectionStyle = .none
        self.lblTrackingNumber.text = data.postTitle ?? "-"
        self.lblCity.text = data.wpcargoReceiverCity ?? "-"
        self.lblPaymentMode.text = data.paymentWpcargoModeField ?? "-"
        self.lblStatus.text = data.wpcargoStatus ?? "-"
        self.lblCashOnDelivery.text = data.cashOnDel ?? "-"
    }
}
