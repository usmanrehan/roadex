//
//  Home.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 27/01/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit
import ObjectMapper

enum ShipmentType{
    case ReadyForPickup
    case CurrentOrder
    case OrderHistory
}

class Home: BaseViewController {

    @IBOutlet weak var separator1: UIView!
    @IBOutlet weak var separator2: UIView!
    @IBOutlet weak var separator3: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var arrAllShipmentsModel =  [AllShipmentsModel]()
    var arrReadyForPickup =  [AllShipmentsModel]()
    var arrCurrentOrder   =  [AllShipmentsModel]()
    var arrOrderHistory   =  [AllShipmentsModel]()
    
    var shipmentType = ShipmentType.ReadyForPickup
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllShipments()
    }
    
    @IBAction func onBtnSideMenu(_ sender: UIBarButtonItem) {
        self.sideMenuController?.showLeftView()
    }
    @IBAction func onBtnAddShipment(_ sender: UIBarButtonItem) {
        self.pushToAddNewShipment()
    }
    @IBAction func onBtnReadyForPickup(_ sender: UIButton) {
        self.shipmentType = .ReadyForPickup
        self.setUI()
    }
    @IBAction func onBtnCurrentOrder(_ sender: UIButton) {
        self.shipmentType = .CurrentOrder
        self.setUI()
    }
    @IBAction func onBtnOrderHistory(_ sender: UIButton) {
        self.shipmentType = .OrderHistory
        self.setUI()
    }
}
//MARK:- Helper Methods
extension Home{
    private func setUI(){
        switch self.shipmentType {
        case .ReadyForPickup:
            self.separator1.alpha = 1.0
            self.separator2.alpha = 0.0
            self.separator3.alpha = 0.0
        case .CurrentOrder:
            self.separator1.alpha = 0.0
            self.separator2.alpha = 1.0
            self.separator3.alpha = 0.0
        case .OrderHistory:
            self.separator1.alpha = 0.0
            self.separator2.alpha = 0.0
            self.separator3.alpha = 1.0
        }
        self.tableView.reloadData()
    }
    private func pushToShipmentDetail(data:AllShipmentsModel){
        let storyboard = AppStoryboard.Main.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ShipmentDetail") as! ShipmentDetail
        controller.shipment = data
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToAddNewShipment(){
        let storyboard = AppStoryboard.Main.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "NewShipment") as! NewShipment
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension Home:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.shipmentType {
        case .ReadyForPickup:
            return self.arrReadyForPickup.count
        case .CurrentOrder:
            return self.arrCurrentOrder.count
        case .OrderHistory:
            return self.arrOrderHistory.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipmentTVC", for: indexPath) as! ShipmentTVC
        switch self.shipmentType {
        case .ReadyForPickup:
            cell.setData(data: self.arrReadyForPickup[index])
        case .CurrentOrder:
            cell.setData(data: self.arrCurrentOrder[index])
        case .OrderHistory:
            cell.setData(data: self.arrOrderHistory[index])
        }
        cell.btnView.tag = index
        cell.btnView.addTarget(self, action: #selector(self.onBtnViewDetail(sender:)), for: .touchUpInside)
        return cell
    }
    @objc func onBtnViewDetail(sender:UIButton){
        let index = sender.tag
        var data = AllShipmentsModel()
        switch self.shipmentType {
        case .ReadyForPickup:
            data = self.arrReadyForPickup[index]
        case .CurrentOrder:
            data = self.arrCurrentOrder[index]
        case .OrderHistory:
            data = self.arrOrderHistory[index]
        }
        self.pushToShipmentDetail(data: data)
    }
}
extension Home{
    private func getAllShipments(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/all"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getArrayResponse(endPoint: endPoint, params: [:], success: { (responseArray) in
            Utility.hideLoader()
            self.arrReadyForPickup.removeAll()
            self.arrCurrentOrder.removeAll()
            self.arrOrderHistory.removeAll()
            self.arrAllShipmentsModel = Mapper<AllShipmentsModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            for item in self.arrAllShipmentsModel{
                switch (item.wpcargoStatus ?? "").lowercased(){
                case "ready for pickup":
                    self.arrReadyForPickup.append(item)
                case "picked up","in hub","out for delivery","reschedule with client","customer refused order","delivered","customer unreachable","shipper request to take back the shipment":
                    self.arrCurrentOrder.append(item)
                case "payment received","returned":
                    self.arrOrderHistory.append(item)
                default:
                    break
                }
            }
            print(self.arrAllShipmentsModel)
            self.setUI()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
