//
//  ShipmentTVC.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 08/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

class ShipmentTVC: UITableViewCell {

    @IBOutlet weak var lblShipmentNumber: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    func setData(data:AllShipmentsModel){
        self.selectionStyle = .none
        self.lblShipmentNumber.text = data.postTitle ?? "-"
        self.lblStatus.text = data.wpcargoStatus ?? "-"
    }
    
}
