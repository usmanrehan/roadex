//
//  Login.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 27/01/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit
import ObjectMapper

class Login: BaseViewController {

    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tfUserName.text = "develop2010"
//        self.tfPassword.text = "develop2010"
        
//        self.tfUserName.text = "Dominic"
//        self.tfPassword.text = "asdfasdf"
        
//        self.tfUserName.text = "Hader mohamed hassan"
//        self.tfPassword.text = "123456"
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnLogin(_ sender: UIButton) {
        self.validate()
    }
    @IBAction func onBtnSignUp(_ sender: UIButton) {
        self.pushToSignUp()
    }
    
}
//MARK:- Helper methods
extension Login{
    private func validate(){
        let userName = self.tfUserName.text ?? ""
        let password = self.tfPassword.text ?? ""
        if !Validation.validateStringLength(userName){
            Utility.main.showToast(message: Strings.INVALID_USER_NAME.rawValue)
            return
        }
        if password.count < 6{
            Utility.main.showToast(message: Strings.INVALID_PASSWORD.rawValue)
            return
        }
        self.login()
    }
    private func pushToSignUp(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "SignUp")
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- Service
extension Login{
    private func login(){
        let endPoint = "custom-plugin/login"
        let username = self.tfUserName.text ?? ""
        let password = self.tfPassword.text ?? ""
        let params:[String:Any] = ["username":username,
                                   "password":password]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getDictionaryResponse(endPoint: endPoint, params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            let user = Mapper<User>().map(JSON: response["data"] as! [String:Any]) ?? User()
            if (user.cargoApi ?? "").isEmpty{
                Utility.showAlert(errorMessage: "\(String(describing: response["data"]))")
                return
            }
            try! Global.APP_REALM?.write(){
                AppStateManager.sharedInstance.loggedInUser = user
                Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: .all)
            }
            AppDelegate.shared.changeRootViewController()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
            Utility.showAlert(errorMessage: "Invalid credentials")
        }
    }
}
