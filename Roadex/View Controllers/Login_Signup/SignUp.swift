//
//  SignUp.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 27/01/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

class SignUp: BaseViewController {

    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var dropDownAnchor: UIView!
    
    var arrCities = [String]()
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.getCities()
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnSignUp(_ sender: UIButton) {
        self.validate()
    }
    @IBAction func onBtnLogin(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnCity(_ sender: UIButton) {
        self.dropDown.show()
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfCity.text = self.arrCities[index]
        }
    }
}

//MARK:- Helper Methods
extension SignUp{
    private func validate(){
        let userName = self.tfUserName.text ?? ""
        let emailAddress = self.tfEmailAddress.text ?? ""
        let phoneNumber = self.tfPhoneNumber.text ?? ""
        let address = self.tfAddress.text ?? ""
        let city = self.tfCity.text ?? ""
        let password = self.tfPassword.text ?? ""
        let confirmPassword = self.tfConfirmPassword.text ?? ""
        if !Validation.validateStringLength(userName){
            Utility.main.showToast(message: Strings.INVALID_USER_NAME.rawValue)
            return
        }
        if !Validation.isValidEmail(emailAddress){
            Utility.main.showToast(message: Strings.INVALID_EMAIL.rawValue)
            return
        }
        if !Validation.validateStringLength(phoneNumber){
            Utility.main.showToast(message: Strings.INVALID_PHONE.rawValue)
            return
        }
        if !Validation.validateStringLength(address){
            Utility.main.showToast(message: Strings.INVALID_ADDRESS.rawValue)
            return
        }
        if !Validation.validateStringLength(city){
            Utility.main.showToast(message: Strings.INVALID_CITY.rawValue)
            return
        }
        if password.count < 6{
            Utility.main.showToast(message: Strings.INVALID_PASSWORD.rawValue)
            return
        }
        if password != confirmPassword{
            Utility.main.showToast(message: Strings.INVALID_PASSWORD_AND_CONFIRM_PASSWORD.rawValue)
            return
        }
        self.signUp()
    }
}
//MARK:- Services
extension SignUp{
    private func signUp(){
        let endPoint = "wp/v2/users/register"
        let username = self.tfUserName.text ?? ""
        let email = self.tfEmailAddress.text ?? ""
        let phone = self.tfPhoneNumber.text ?? ""
        let address = self.tfAddress.text ?? ""
        let city = self.tfCity.text ?? ""
        let password = self.tfPassword.text ?? ""
        let params:[String:Any] = ["username":username,
                                   "email":email,
                                   "phone":phone,
                                   "address":address,
                                   "city":city,
                                   "password":password]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.postDictionaryResponse(endPoint: endPoint, params: params, success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            Utility.main.showAlert(message: "Signup successfull", title: "Success", controller: self, usingCompletionHandler: {
                AppDelegate.shared.changeRootViewController()
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getCities(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/shipment/fields"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getDictionaryResponse(endPoint: endPoint, params: [:], success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            if let receiver_info = response["receiver_info"] as? NSArray{
                for item in receiver_info{
                    guard let obj = item as? NSDictionary else {return}
                    guard let label = obj["label"] as? String else {return}
                    if label == "City"{
                        guard let cities = obj["field_data"] as? [String] else {return}
                        self.arrCities = cities
                        self.dropDown.dataSource = cities
                        break
                    }
                }
            }
            
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}

