//
//  NewShipment.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 11/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit
import ObjectMapper

struct ShipperInfo {
    var shipperName = ""
    var shipperEmail = ""
    var shipperPhoneNumber = ""
    var shipperAddress = ""
    var shipperCity = ""
}
struct ReceiverInfo {
    var receiverName = ""
    var receiverPhoneNumber = ""
    var receiverAddress = ""
    var receiverCity = ""
}
struct PackageInfo {
    var packageQuantity = ""
    var packagePieceType = ""
    var packageLength = ""
    var packageWidth = ""
    var packageHeight = ""
}
struct MoreInfo {
    var cashOnDelivery = ""
    var weight = ""
    var description = ""
}

class NewShipment: BaseViewController {
    
    @IBOutlet weak var pieceTypeAnchor: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var shipperInfo = ShipperInfo()
    var receiverInfo = ReceiverInfo()
    var arrPackageInfo: [PackageInfo] = [PackageInfo()]
    var moreInfo = MoreInfo()
    var arrCities = [String]()
    var arrPieceType = ["Small","Medium","Large"]
    var shipperNameDropDown = DropDown()
    var receiverNameDropDown = DropDown()
    var arrShipperNames = [String]()
    var arrReceiverNames = [String]()
    var arrFilterShipperNames = [String]()
    var arrFilterReceiverNames = [String]()
    var arrShipperAddressBookModel = [ShipperAddressBookModel]()
    var arrReceiverAddressBookModel = [ReceiverAddressBookModel]()
    var arrFilterShipperAddressBookModel = [ShipperAddressBookModel]()
    var arrFilterReceiverAddressBookModel = [ReceiverAddressBookModel]()
    var shipperCityDropDown = DropDown()
    var receiverCityDropDown = DropDown()
    var pieceTypeDropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
        self.getShipperBook()
        self.getReceiverBook()
        self.pieceTypeDropDown.dataSource = self.arrPieceType
        self.pieceTypeDropDown.anchorView = pieceTypeAnchor
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension NewShipment:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,1,3:
            return 1
        case 2:
            return self.arrPackageInfo.count
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShipperInfoTVC", for: indexPath) as! ShipperInfoTVC
            cell.selectionStyle = .none
            cell.tfShipperName.tag = 1
            cell.tfEmail.tag = 2
            cell.tfPhoneNumber.tag = 3
            cell.tfAddress.tag = 4
            cell.tfCity.tag = 5
            cell.tfShipperName.addTarget(self, action: #selector(onTfShipperInfo(sender:)), for: .editingChanged)
            cell.tfEmail.addTarget(self, action: #selector(onTfShipperInfo(sender:)), for: .editingChanged)
            cell.tfPhoneNumber.addTarget(self, action: #selector(onTfShipperInfo(sender:)), for: .editingChanged)
            cell.tfAddress.addTarget(self, action: #selector(onTfShipperInfo(sender:)), for: .editingChanged)
            cell.tfCity.addTarget(self, action: #selector(onTfShipperInfo(sender:)), for: .editingChanged)
            cell.tfShipperName.text = self.shipperInfo.shipperName
            cell.tfEmail.text = self.shipperInfo.shipperEmail
            cell.tfPhoneNumber.text = self.shipperInfo.shipperPhoneNumber
            cell.tfAddress.text = self.shipperInfo.shipperAddress
            cell.tfCity.text = self.shipperInfo.shipperCity
            cell.btnCity.addTarget(self, action: #selector(self.onBtnShipperCity(sender:)), for: .touchUpInside)
            self.shipperNameDropDown.anchorView = cell.viewShipperNameAnchor
            self.shipperCityDropDown.anchorView = cell.viewCity
            self.shipperNameDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.shipperInfo.shipperName = self.arrFilterShipperAddressBookModel[index].wpcargoShipperName ?? ""
                self.shipperInfo.shipperPhoneNumber = self.arrFilterShipperAddressBookModel[index].wpcargoShipperPhone ?? ""
                self.shipperInfo.shipperAddress = self.arrFilterShipperAddressBookModel[index].wpcargoShipperAddress ?? ""
                cell.tfShipperName.text = self.shipperInfo.shipperName
                cell.tfPhoneNumber.text = self.shipperInfo.shipperPhoneNumber
                cell.tfAddress.text = self.shipperInfo.shipperAddress
            }
            self.shipperCityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.shipperInfo.shipperCity = self.arrCities[index]
                cell.tfCity.text = self.shipperInfo.shipperCity
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverInfoTVC", for: indexPath) as! ReceiverInfoTVC
            cell.selectionStyle = .none
            cell.tfReceiverName.tag = 1
            cell.tfPhoneNumber.tag = 2
            cell.tfAddress.tag = 3
            cell.tfCity.tag = 4
            cell.tfReceiverName.addTarget(self, action: #selector(onTfReceiverInfo(sender:)), for: .editingChanged)
            cell.tfPhoneNumber.addTarget(self, action: #selector(onTfReceiverInfo(sender:)), for: .editingChanged)
            cell.tfAddress.addTarget(self, action: #selector(onTfReceiverInfo(sender:)), for: .editingChanged)
            cell.tfCity.addTarget(self, action: #selector(onTfReceiverInfo(sender:)), for: .editingChanged)
            cell.tfReceiverName.text = self.receiverInfo.receiverName
            cell.tfPhoneNumber.text = self.receiverInfo.receiverPhoneNumber
            cell.tfAddress.text = self.receiverInfo.receiverAddress
            cell.tfCity.text = self.receiverInfo.receiverCity
            cell.btnCity.addTarget(self, action: #selector(self.onBtnReceiverCity(sender:)), for: .touchUpInside)
            self.receiverNameDropDown.anchorView = cell.viewReceiverNameAnchor
            self.receiverCityDropDown.anchorView = cell.viewCity
            self.receiverNameDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.receiverInfo.receiverName = self.arrFilterReceiverAddressBookModel[index].wpcargoReceiverName ?? ""
                self.receiverInfo.receiverPhoneNumber = self.arrFilterReceiverAddressBookModel[index].wpcargoReceiverPhone ?? ""
                self.receiverInfo.receiverAddress = self.arrFilterReceiverAddressBookModel[index].wpcargoReceiverAddress ?? ""
                cell.tfReceiverName.text = self.receiverInfo.receiverName
                cell.tfPhoneNumber.text = self.receiverInfo.receiverPhoneNumber
                cell.tfAddress.text = self.receiverInfo.receiverAddress
            }
            self.receiverCityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.receiverInfo.receiverCity = self.arrCities[index]
                cell.tfCity.text = self.receiverInfo.receiverCity
            }
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PackageInfoTVC", for: indexPath) as! PackageInfoTVC
            cell.selectionStyle = .none
            let index = indexPath.row
            cell.tfQuantity.tag = index
            cell.tfPieceType.tag = index
            cell.btnPieceType.tag = index
            cell.tfLength.tag = index
            cell.tfWidth.tag = index
            cell.tfHeight.tag = index
            
            cell.tfQuantity.addTarget(self, action: #selector(onTfPackageQuantity(sender:)), for: .editingChanged)
            cell.tfPieceType.addTarget(self, action: #selector(onTfPackagePieceType(sender:)), for: .editingChanged)
            cell.btnPieceType.addTarget(self, action: #selector(onBtnPieceType(sender:)), for: .touchUpInside)
            cell.tfLength.addTarget(self, action: #selector(onTfPackageLength(sender:)), for: .editingChanged)
            cell.tfWidth.addTarget(self, action: #selector(onTfPackageWidth(sender:)), for: .editingChanged)
            cell.tfHeight.addTarget(self, action: #selector(onTfPackageHeight(sender:)), for: .editingChanged)
            
            let data = self.arrPackageInfo[index]
            cell.tfQuantity.text = data.packageQuantity
            cell.tfPieceType.text = data.packagePieceType
            cell.tfLength.text = data.packageLength
            cell.tfWidth.text = data.packageWidth
            cell.tfHeight.text = data.packageHeight

            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInfoTVC", for: indexPath) as! MoreInfoTVC
            cell.selectionStyle = .none
            cell.btnAddMorePackageInfo.addTarget(self, action: #selector(self.onBtnMorePackageInfo(sender:)), for: .touchUpInside)
            cell.tfCashOnDelivery.addTarget(self, action: #selector(onTfCashOnDelivery(sender:)), for: .editingChanged)
            cell.tfWeight.addTarget(self, action: #selector(onTfWeight(sender:)), for: .editingChanged)
            cell.tvPackageDescription.delegate = self
            cell.btnDone.addTarget(self, action: #selector(self.onBtnDone(sender:)), for: .touchUpInside)
            cell.tfCashOnDelivery.text = self.moreInfo.cashOnDelivery
            cell.tfWeight.text = self.moreInfo.weight
            cell.tvPackageDescription.text = self.moreInfo.description
            return cell
        default:
            return UITableViewCell()
        }
    }
   
}
//MARK:- Helper MEthods
extension NewShipment{
    @objc func onTfShipperInfo(sender:UITextField){
        switch sender.tag {
        case 1:
            self.shipperInfo.shipperName = sender.text ?? ""
            self.arrFilterShipperNames = self.arrShipperNames.filter{ $0.localizedCaseInsensitiveContains(sender.text ?? "") }
            self.arrFilterShipperAddressBookModel = self.arrShipperAddressBookModel.filter{ ($0.wpcargoShipperName?.localizedCaseInsensitiveContains(sender.text ?? ""))! }
            self.shipperNameDropDown.dataSource = self.arrFilterShipperNames
            self.shipperNameDropDown.show()
        case 2:
            self.shipperInfo.shipperEmail = sender.text ?? ""
        case 3:
            self.shipperInfo.shipperPhoneNumber = sender.text ?? ""
        case 4:
            self.shipperInfo.shipperAddress = sender.text ?? ""
        case 5:
            self.shipperInfo.shipperCity = sender.text ?? ""
        default:
            break
        }
    }
    @objc func onTfReceiverInfo(sender:UITextField){
        switch sender.tag {
        case 1:
            self.receiverInfo.receiverName = sender.text ?? ""
            self.arrFilterReceiverNames = self.arrReceiverNames.filter{ $0.localizedCaseInsensitiveContains(sender.text ?? "") }
            self.arrFilterReceiverAddressBookModel = self.arrReceiverAddressBookModel.filter{ ($0.wpcargoReceiverName?.localizedCaseInsensitiveContains(sender.text ?? ""))! }
            self.receiverNameDropDown.dataSource = self.arrFilterReceiverNames
            self.receiverNameDropDown.show()
        case 2:
            self.receiverInfo.receiverPhoneNumber = sender.text ?? ""
        case 3:
            self.receiverInfo.receiverAddress = sender.text ?? ""
        case 4:
            self.receiverInfo.receiverCity = sender.text ?? ""
        default:
            break
        }
    }
    @objc func onTfPackageQuantity(sender:UITextField){
        self.arrPackageInfo[sender.tag].packageQuantity = sender.text ?? ""
    }
    @objc func onTfPackagePieceType(sender:UITextField){
        self.arrPackageInfo[sender.tag].packagePieceType = sender.text ?? ""
    }
    @objc func onTfPackageLength(sender:UITextField){
        self.arrPackageInfo[sender.tag].packageLength = sender.text ?? ""
    }
    @objc func onTfPackageWidth(sender:UITextField){
        self.arrPackageInfo[sender.tag].packageWidth = sender.text ?? ""
    }
    @objc func onTfPackageHeight(sender:UITextField){
        self.arrPackageInfo[sender.tag].packageHeight = sender.text ?? ""
    }
    @objc func onBtnMorePackageInfo(sender:UIButton){
        self.arrPackageInfo.append(PackageInfo())
        self.tableView.reloadSections([2], with: .none)
    }
    @objc func onTfCashOnDelivery(sender:UITextField){
        self.moreInfo.cashOnDelivery = sender.text ?? ""
    }
    @objc func onTfWeight(sender:UITextField){
        self.moreInfo.weight = sender.text ?? ""
    }
    @objc func onBtnShipperCity(sender:UIButton){
        self.shipperCityDropDown.show()
    }
    @objc func onBtnReceiverCity(sender:UIButton){
        self.receiverCityDropDown.show()
    }
    @objc func onBtnPieceType(sender:UIButton){
        self.pieceTypeDropDown.show()
        self.pieceTypeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.arrPackageInfo[sender.tag].packagePieceType = self.arrPieceType[index]
            self.tableView.reloadSections([2], with: .none)
            self.tableView.scrollToNearestSelectedRow(at: .middle, animated: false)
        }
    }
    @objc func onBtnDone(sender:UIButton){
        self.addNewShipment()
    }
    private func getParams()->[String:String]{
        let shipper = self.shipperInfo
        let receiver = self.receiverInfo
        let packages = self.arrPackageInfo
        let more = self.moreInfo
        
        var params = [String:String]()
        
        let wpcargo_shipper_name = shipper.shipperName
        let wpcargo_shipper_phone = shipper.shipperPhoneNumber
        let wpcargo_shipper_address = shipper.shipperAddress
        let wpcargo_shipper_city = shipper.shipperCity
        let wpcargo_shipper_email = shipper.shipperEmail
        let wpcargo_type_of_shipment = "Express"
        let payment_wpcargo_mode_field = "Cash on Delivery"
        let wpcargo_destination = receiver.receiverAddress
        let wpcargo_pickup_date_picker = ""
        let wpcargo_pickup_time_picker = ""
        let wpcargo_status = "Ready for pickup"
        let wpcargo_expected_delivery_date_picker = ""
        let open_shipment = "Allow customer to open shipment"
        let wpcargo_comments = more.description
        let cash_on_del = more.cashOnDelivery
        let wpcargo_receiver_name = receiver.receiverName
        let wpcargo_receiver_phone = receiver.receiverPhoneNumber
        let wpcargo_receiver_address = receiver.receiverAddress
        let wpcargo_receiver_city = receiver.receiverCity
        let wpcargo_receiver_address2 = receiver.receiverAddress
        let wpcargo_weight = more.weight
        let wpcargo_order_note3 = more.description
        
        params = [
            "wpcargo_shipper_name" : wpcargo_shipper_name,
            "wpcargo_shipper_phone" : wpcargo_shipper_phone,
            "wpcargo_shipper_address" : wpcargo_shipper_address,
            "wpcargo_shipper_city" : wpcargo_shipper_city,
            "wpcargo_shipper_email" : wpcargo_shipper_email,
            "wpcargo_type_of_shipment" : wpcargo_type_of_shipment,
            "payment_wpcargo_mode_field" : payment_wpcargo_mode_field,
            "wpcargo_destination" : wpcargo_destination,
            "wpcargo_pickup_date_picker" : wpcargo_pickup_date_picker,
            "wpcargo_pickup_time_picker" : wpcargo_pickup_time_picker,
            "wpcargo_status" : wpcargo_status,
            "wpcargo_expected_delivery_date_picker" : wpcargo_expected_delivery_date_picker,
            "open_shipment" : open_shipment,
            "wpcargo_comments" : wpcargo_comments,
            "cash_on_del" : cash_on_del,
            "wpcargo_receiver_name" : wpcargo_receiver_name,
            "wpcargo_receiver_phone" : wpcargo_receiver_phone,
            "wpcargo_receiver_address" : wpcargo_receiver_address,
            "wpcargo_receiver_city" : wpcargo_receiver_city,
            "wpcargo_receiver_address2" : wpcargo_receiver_address2,
            "wpcargo_weight" : wpcargo_weight,
            "wpcargo_order_note3" : wpcargo_order_note3
        ]
        for (index,package) in packages.enumerated(){
            params["pq_package_items[\(index)][qty]"] = package.packageQuantity
            params["pq_package_items[\(index)][piece_type]"] = package.packagePieceType
            params["pq_package_items[\(index)][length]"] = package.packageLength
            params["pq_package_items[\(index)][width]"] = package.packageWidth
            params["pq_package_items[\(index)][height]"] = package.packageHeight
            params["pq_package_items[\(index)][description]"] = ""
        }
        return params
    }
}
extension NewShipment:UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        self.moreInfo.description = textView.text
    }
}
//MARK:- Service
extension NewShipment{
    private func addNewShipment(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/shipment/add"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.postDictionaryResponse(endPoint: endPoint, params: self.getParams(), success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            Utility.main.showAlert(message: "Shipment added successfully", title: "Success", controller: self, usingCompletionHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getData(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/shipment/fields"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getDictionaryResponse(endPoint: endPoint, params: [:], success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            print(response)
            if let receiver_info = response["receiver_info"] as? NSArray{
                for item in receiver_info{
                    guard let obj = item as? NSDictionary else {return}
                    guard let label = obj["label"] as? String else {return}
                    if label == "City"{
                        guard let cities = obj["field_data"] as? [String] else {return}
                        self.arrCities = cities
                        self.shipperCityDropDown.dataSource = cities
                        self.receiverCityDropDown.dataSource = cities
                        break
                    }
                }
            }
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
extension NewShipment{
    private func getShipperBook(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/address/shipper/all"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getArrayResponse(endPoint: endPoint, params: [:], success: { (responseArray) in
            Utility.hideLoader()
            self.arrShipperAddressBookModel = Mapper<ShipperAddressBookModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            print(self.arrShipperAddressBookModel)
            for address in self.arrShipperAddressBookModel{
                self.arrShipperNames.append(address.wpcargoShipperName ?? "")
            }
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getReceiverBook(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/address/receiver/all"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getArrayResponse(endPoint: endPoint, params: [:], success: { (responseArray) in
            print(responseArray)
            Utility.hideLoader()
            self.arrReceiverAddressBookModel = Mapper<ReceiverAddressBookModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            print(self.arrReceiverAddressBookModel.count)
            for address in self.arrReceiverAddressBookModel{
                self.arrReceiverNames.append(address.wpcargoReceiverName ?? "")
            }
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
