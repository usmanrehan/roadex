//
//  MoreInfoTVC.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 12/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

class MoreInfoTVC: UITableViewCell {

    @IBOutlet weak var btnAddMorePackageInfo: UIButton!
    @IBOutlet weak var tfCashOnDelivery: UITextField!
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var tvPackageDescription: UITextView!
    @IBOutlet weak var btnDone: UIButton!
}
