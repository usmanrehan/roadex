//
//  PackageInfoTVC.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 12/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

class PackageInfoTVC: UITableViewCell {

    @IBOutlet weak var tfQuantity: UITextField!
    @IBOutlet weak var tfPieceType: UITextField!
    @IBOutlet weak var btnPieceType: UIButton!
    @IBOutlet weak var tfLength: UITextField!
    @IBOutlet weak var tfWidth: UITextField!
    @IBOutlet weak var tfHeight: UITextField!
    

}
