//
//  ShipperInfoTVC.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 12/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

class ShipperInfoTVC: UITableViewCell {

    @IBOutlet weak var tfShipperName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var viewShipperNameAnchor: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var btnCity: UIButton!
}
