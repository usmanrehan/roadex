//
//  ShipmentDetail.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 09/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

class ShipmentDetail: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var shipment = AllShipmentsModel()
    
    @IBAction func onBtnBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ShipmentDetail:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipmentDetailTVC", for: indexPath) as! ShipmentDetailTVC
        cell.setData(data: self.shipment)
        return cell
    }
}
