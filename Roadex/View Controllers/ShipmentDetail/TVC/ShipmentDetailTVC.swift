//
//  ShipmentDetailTVC.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 09/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

class ShipmentDetailTVC: UITableViewCell {

    @IBOutlet weak var imgBarCode: UIImageView!
    @IBOutlet weak var lblShipmentNumber: UILabel!
    @IBOutlet weak var lblShipperPhoneNumber: UILabel!
    @IBOutlet weak var lblShipperName: UILabel!
    @IBOutlet weak var lblShipperAddress: UILabel!
    @IBOutlet weak var lblShipperCity: UILabel!
    @IBOutlet weak var lblReceiverPhoneNumber: UILabel!
    @IBOutlet weak var lblReceiverName: UILabel!
    @IBOutlet weak var lblReceiverAddress: UILabel!
    @IBOutlet weak var lblReceiverAddress2: UILabel!
    @IBOutlet weak var lblReceiverCity: UILabel!
    @IBOutlet weak var lblShipmentStatus: UILabel!
    @IBOutlet weak var lblCourier: UILabel!
    @IBOutlet weak var lblCashOnDelivery: UILabel!
    @IBOutlet weak var lblDoYouHaveAnyNotes: UILabel!
    
    func setData(data:AllShipmentsModel){
        self.selectionStyle = .none
        print(data)
        self.lblShipmentNumber.text = "Shipment #" + (data.postTitle ?? "-")
        self.lblShipperPhoneNumber.text = data.wpcargoShipperPhone ?? "-"
        self.lblShipperName.text = data.registeredShipper ?? "-"
        self.lblShipperAddress.text = data.wpcargoShipperAddress ?? "-"
        self.lblShipperCity.text = data.wpcargoShipperCity ?? "-"
        self.lblReceiverPhoneNumber.text = data.wpcargoReceiverPhone ?? "-"
        self.lblReceiverName.text = data.wpcargoReceiverName ?? "-"
        self.lblReceiverAddress.text = data.wpcargoReceiverAddress ?? "-"
        self.lblReceiverAddress2.text = data.wpcargoReceiverAddress2 ?? "-"
        self.lblReceiverCity.text = data.wpcargoReceiverCity ?? "-"
        self.lblShipmentStatus.text = "Shipment status \(data.wpcargoStatus ?? "-")".uppercased()
        self.lblCourier.text = data.wpcargoCourier ?? "-"
        self.lblCashOnDelivery.text = data.cashOnDel ?? "-"
        self.lblDoYouHaveAnyNotes.text = data.wpcargoOrderNote3 ?? "-"
    }
}
