//
//  SideMenu.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 27/01/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit

class SideMenu: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var arrOptions = [OptionData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrOptions = [OptionData(option: Strings.DASHBOARD.rawValue, isSelected: true),
                           OptionData(option: Strings.ALL_SHIPMENT.rawValue, isSelected: false),
                           OptionData(option: Strings.ADDRESS_BOOK.rawValue, isSelected: false),
                           OptionData(option: Strings.TRACKING.rawValue, isSelected: false),
                           OptionData(option: Strings.LOGOUT.rawValue, isSelected: false)]
        // Do any additional setup after loading the view.
    }
}
extension SideMenu:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC", for: indexPath) as! SideMenuTVC
        cell.setData(data: self.arrOptions[indexPath.row])
        return cell
    }
}
extension SideMenu:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<self.arrOptions.count{
            self.arrOptions[i].isSelected = false
        }
        self.arrOptions[indexPath.row].isSelected = true
        self.tableView.reloadData()
        switch indexPath.row {
        case 0://Dashboard
            self.changeContentTo(controller: "Home")
        case 1://AllShipments
            self.changeContentTo(controller: "AllShipments")
        case 2://AddressBook
            self.changeContentTo(controller: "AddressBook")
        case 3://Tracking
            self.changeContentTo(controller: "Tracking")
        case 4:
            self.showLogoutPopUp()
        default:
            break
        }
    }
}
//MARK:- ChangeContentView
extension SideMenu{
    private func changeContentTo(controller:String){
        let storyboard = AppStoryboard.Main.instance
        let controller = storyboard.instantiateViewController(withIdentifier: controller)
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
        self.sideMenuController?.hideLeftView()
    }
  
    private func showLogoutPopUp(){
        Utility.main.showAlertPermission(message: "Are you sure you want to Logout?", title: "Logout", controller: self) { (actionYes, actionNo) in
            if actionYes != nil {
                AppStateManager.sharedInstance.logoutUser()
            }
        }
    }
}
