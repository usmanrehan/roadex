//
//  SideMenuTVC.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 27/01/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit
struct OptionData {
    var option = ""
    var isSelected = false
}

class SideMenuTVC: UITableViewCell {

    @IBOutlet weak var lblOption: UILabel!
    
    func setData(data:OptionData){
        self.selectionStyle = .none
        self.lblOption.text = data.option
        if data.isSelected{
            self.lblOption.textColor = Global.APP_COLOR
        }
        else{
            self.lblOption.textColor = UIColor.black
        }
    }
}
