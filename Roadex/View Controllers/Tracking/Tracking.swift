//
//  Tracking.swift
//  Roadex
//
//  Created by M Usman Bin Rehan on 10/02/2019.
//  Copyright © 2019 Roadex. All rights reserved.
//

import UIKit
import ObjectMapper

class Tracking: BaseViewController {

    @IBOutlet weak var tfTrackingNumber: UITextField!
    var shipment = AllShipmentsModel()
    
    @IBAction func onBtnSideMenu(_ sender: UIBarButtonItem) {
        self.sideMenuController?.showLeftView()
    }
    @IBAction func onBtnTrackingResult(_ sender: UIButton) {
        let trackingNumber = self.tfTrackingNumber.text ?? ""
        if !Validation.validateStringLength(trackingNumber){
            Utility.main.showToast(message: Strings.INVALID_TRACKING_NUMBER.rawValue)
            return
        }
        self.getShipment()
    }
}
//MARK:- Helper Methods
extension Tracking{
    private func pushToShipmentDetail(data:AllShipmentsModel){
        let storyboard = AppStoryboard.Main.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ShipmentDetail") as! ShipmentDetail
        controller.shipment = data
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- Service
extension Tracking{
    private func getShipment(){
        let cargoApi = AppStateManager.sharedInstance.loggedInUser.cargoApi ?? ""
        let trackingNumber = self.tfTrackingNumber.text ?? ""
        let endPoint = "wpcargo/v1/api/\(cargoApi)/shipment/track/\(trackingNumber)"
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getDictionaryResponse(endPoint: endPoint, params: [:], success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            self.shipment = Mapper<AllShipmentsModel>().map(JSON:responseObject) ?? AllShipmentsModel()
            self.pushToShipmentDetail(data: self.shipment)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
